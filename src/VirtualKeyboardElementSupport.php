<?php

namespace Drupal\virtual_keyboard;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a virtual keyboard element support.
 */
class VirtualKeyboardElementSupport {

  /**
   * {@inheritdoc}
   */
  public function attach(&$element) {
    $config = $this->configFactory->get('virtual_keyboard.settings');

    $focus = in_array($config->get('method'), ['focus', 'icon_and_focus']) ? 'focus' : NULL;

    $options = [
      // Buttons layout for virtual keyboard.
      'layout' => 'qwerty',
      // When cliked outside keyboard leave keyboard shown.
      'stayOpen' => FALSE,
      // Don't have to click "Accept" button.
      'autoAccept' => TRUE,
      // Use the same textfield.
      'usePreview' => FALSE,
      // Method of opening keyboard.
      'openOn' => $focus,
      // Position of virtual keyboard relative to textfield.
      'position' => [
        'my' => 'left top',
        'at' => 'left bottom',
        'at2' => 'left bottom',
      ],
    ];

    $this->moduleHandler->alter('virtual_keyboard_options', $options);

    $element['#attached']['library'][] = 'virtual_keyboard/virtual_keyboard';

    // Adding layouts.
    $layouts = $this->moduleHandler->invokeAll('virtual_keyboard_layout_info');
    // Allow modules to alter keyboard layouts.
    $this->moduleHandler->alter('virtual_keyboard_layout_info', $layouts);

    // Adding settings.
    $whitechars = ["\r\n", "\r", "\n"];
    $element['#attached']['drupalSettings']['virtual_keyboard'] = [
      'include' => str_replace($whitechars, ',', $config->get('included_selectors')),
      'exclude' => str_replace($whitechars, ',', $config->get('excluded_selectors')),
      'method' => $config->get('method'),
      'options' => $options,
      'layouts' => $layouts,
    ];
  }

  /**
   * Virtual Keyboard Element Support constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config service.
   * @param \Drupal\scic_sr_transcoding\Service\ModuleHandlerInterface $moduleHandler
   *   Module handler service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ModuleHandlerInterface $moduleHandler) {
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
  }

}
