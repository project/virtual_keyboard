<?php

namespace Drupal\virtual_keyboard;

use Drupal\Core\Render\Element\RenderCallbackInterface;

/**
 * Provides a trusted callback to alter the virtual keyboard element info.
 *
 * @see virtual_keyboard_element_info_alter()
 */
class VirtualKeyboardInfoAlterCallback implements RenderCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function preRender($element) {
    if (\Drupal::currentUser()->hasPermission('use virtual keyboard')) {
      if (isset($element['#virtual_keyboard'])) {
        $action = $element['#virtual_keyboard'] == TRUE ? 'include' : 'exclude';

        if ($element['#type'] == 'form') {
          $element['#attributes']['class'][] = 'virtual-keyboard-' . $action . '-children';
        }
        else {
          $element['#attributes']['class'][] = 'virtual-keyboard-' . $action;
        }
      }

      if ($element['#type'] == 'form') {
        \Drupal::service('virtual_keyboard.element_support')->attach($element);
      }
    }

    return $element;
  }

}
