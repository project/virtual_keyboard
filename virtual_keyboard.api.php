<?php

/**
 * @file
 * Documents API functions for Virtual Keyboard module.
 */

/**
 * Alter the options used by Virtual Keyboard plugin.
 *
 * See https://github.com/Mottie/Keyboard/wiki/Options for further information.
 *
 * @param array $options
 *   Options used by Virtual Keyboard jQuery plugin.
 */
function hook_virtual_keyboard_options_alter(array &$options): void {
  // Use custom layout instead of default qwerty layout.
  $options['layout'] = 'custom';
  $options['customLayout'] = [
    'default' => [
      'C D E F',
      '8 9 A B',
      '4 5 6 7',
      '0 1 2 3',
      '{bksp} {a} {c}',
    ],
  ];
}

/**
 * Registers layouts to be used with Virtual Keyboard.
 *
 * @return array
 *   Registered keyboard layouts.
 */
function hook_virtual_keyboard_layout_info(): array {
  // Add numeric keyboard layout.
  $layouts = [];
  $layouts['num'] = [
    'default' => [
      '= ( ) {b}',
      '{clear} / * -',
      '7 8 9 +',
      '4 5 6 {sign}',
      '1 2 3 %',
      '0 . {a} {c}',
    ],
  ];
  return $layouts;
}

/**
 * Alters layouts used by Virtual Keyboard.
 *
 * @param array $layouts
 *   Virtual keyboard available layouts.
 */
function hook_virtual_keyboard_layout_info_alter(array &$layouts): void {
  // Remove row with numbers from qwerty layout.
  if (isset($layouts['qwerty'])) {
    $layouts['qwerty'] = [
      'default' => [
        // '` 1 2 3 4 5 6 7 8 9 0 - = {bksp}',
        '{tab} q w e r t y u i o p [ ] \\',
        'a s d f g h j k l ; \' {enter}',
        '{shift} z x c v b n m , . / {shift}',
        '{accept} {space} {cancel}',
      ],
      'shift' => [
        // '~ ! @ # $ % ^ & * ( ) _ + {bksp}',
        '{tab} Q W E R T Y U I O P { } |',
        'A S D F G H J K L : " {enter}',
        '{shift} Z X C V B N M < > ? {shift}',
        '{accept} {space} {cancel}',
      ],
    ];
  }
}
