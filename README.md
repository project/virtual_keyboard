## Virtual Keyboard

Attach virtual keyboard to a text fields for mouse-driven entry.

### Features

* Add possibility to fill text fields by virtual keyboard.
* Targeted text-fields can be entered in textarea as CSS selectors.
* It's possible to override default keyboard options
(e.g. keyboard keys) using hook_virtual_keyboard_options_alter().

### Requirements

No special requirements at this time.

### Install

It's recommended to install via composer.

```bash
composer require drupal/virtual_keyboard
drush en virtual_keyboard
```

### Setup/Configuration

1. Install module via composer method.
2. Enable "Virtual Keyboard" module in the modules list of your site.
3. Go to configuration page at "/admin/config/user-interface/virtual-keyboard".
4. Specify CSS selectors for text fields you want to add a virtual keyboard.

### Maintainers

* George Anderson (geoanders) - https://www.drupal.org/u/geoanders
* Pablo (pgrandeg) - https://www.drupal.org/u/pgrandeg
